#/bin/bash

current_version=v2.4

# Build Docker images with tag "latest"
docker build gnu -t registry.gitlab.com/et-program/et/ubuntu-mkl-gnu-for-et:latest
docker build intel -t registry.gitlab.com/et-program/et/ubuntu-mkl-intel-for-et:latest

# Create new tag "v2.3" (increment this as needed) based on the already created tag "latest"
docker tag registry.gitlab.com/et-program/et/ubuntu-mkl-gnu-for-et:latest registry.gitlab.com/et-program/et/ubuntu-mkl-gnu-for-et:${current_version}
docker tag registry.gitlab.com/et-program/et/ubuntu-mkl-intel-for-et:latest registry.gitlab.com/et-program/et/ubuntu-mkl-intel-for-et:${current_version}

# Now you have two images ready to be pushed
# NB! the eT CI/CD pipeline will use "latest" by default
# Therefore: only push "latest" when the image has been properly tested

# Login: use username and Gitlab token with write/read registries access
docker login registry.gitlab.com 

# Push new version - can be used in merge requests for testing by changing the 
# first line in the Gitlab yml file (append ":current-version" to the image specification)
docker push registry.gitlab.com/et-program/et/ubuntu-mkl-gnu-for-et:${current_version}
docker push registry.gitlab.com/et-program/et/ubuntu-mkl-intel-for-et:${current_version}

# Push new version as latest - from now on all eT pipelines will use the new Docker image
docker push registry.gitlab.com/et-program/et/ubuntu-mkl-gnu-for-et:latest
docker push registry.gitlab.com/et-program/et/ubuntu-mkl-intel-for-et:latest
